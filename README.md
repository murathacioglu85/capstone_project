# CAPSTONE PROJECT
## DOWNLOAD FILES WITH URL AND KEEP TRACK OF DOWNLOADED ITEMS & EXTRACT TABLES FROM A WEBSITE.
The application offers two features. First one provides a service to download files with a given URL and save the download history to show it on screen while the second one facilitates the table extraction from a website.
### DownloadURL Activity
1. This Activity UI is opened by clicking on �DOWNLOAD WITH URL BUTTON� on the main activity screen.
2. One text field and three buttons are shown on the screen whereas one scroll view is reserved underneath for the data obtained after clicking on �VIEWHISTORY� button.
3. The text field is provided with a default URL: https://gitlab.com/murathacioglu85/capstone_project/-/raw/fa67d10c763ad2f7309f960af1350585f6d34c58/README.pdf
4. The user can change the URL and download a file as he/she desires
5. When clicked on �DOWNLOAD� button which is the workhorse of this activity, an intent is sent to DownloadURLservice with the URL info added. 
6. The URL is downloaded by the service and Content Provider feature of Android is invoked by the service to create a database and insert the filename and the download time to keep track of the history.
7. A toast pops up when the download starts on the screen as �download started�.
8. Service sends an intent which includes the URI info of the file directory and the status of the download for broadcast receiver to publish whether the download is successful or not. At this stage, on the screen, it appears either �Download complete. Download URI:�..� or �Download failed�. Upon the completion of download, a snack bar pops up to open the file.
9. Afterwards, the content provider URI is shown on the screen with a toast view.
10. �GENERATE DOWNLOAD HISTORY TXT� button creates a txt file which simply comprises of the data in the database. This file is in the same directory as the downloaded items. A snackbar pops up to open the generated txt file.
11. �VIEWHISTORY� button retrieves the sql info and shows is on the screen at the bottom. The view is scrollable both horizontally and vertically.
12. In a nutshell, this feature of the application makes use of Activity, Intent Service, Broadcast Receiver and Content provider to perform the abovementioned tasks. 
### WebScraping Activity-Table Extraction from a given web site
1. By clicking �WEBSCRAPING-TABLES�, the activity screen pops up. 
2. One text field, one button and a dropdown menu item are shown on the screen. There is also a text field defined for the tables to be extracted.
3. The default URL for this activity is https://en.wikipedia.org/wiki/List_of_New_York_area_codes
4. The user can insert another URL of his/her choosing and get the tables of that website.
5. When clicked on �EXTRACT TABLES�, bound service connection is established  and the method defined in JsoupHTMLparser class is invoked. The callable method is used no to hinder the UI thread considering that the parsing may take some time.
6. After the work is done, the tables are extracted and the list for dropdown menu is created.
7. Selecting the table in dropdown menu brings up the table on the screen below with the scrollable view both horizontally and vertically.
8. This feature of the app uses Activity and Bound service to perform the abovementioned tasks.
### Testing
1. Integration, Junit and UI Testing have been implemented for the application.
2. ContentProvider Junit test has been applied to check whether the methods works properly or not. The methods that have been checked are as follows:
      * Insert
      * Query
      * getType
      * delete
      * update

3. Integration test for DownloadURLservice has been conducted to see makeintent method works as intended or not. Unit test for checkURL method has been performed as well.
4. As for the Activities, Espresso test module has been used to check the UI works as intended.
5. JSoupHTMLParser Bound service has been checked via Integration test.
6. All the functionality has been checked by means of the tests mentioned above.






