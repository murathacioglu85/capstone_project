package org.coursera.sustainableapps.capstone_project;


import android.content.ContentResolver;
import android.content.ContentValues;

import android.database.Cursor;
import android.net.Uri;



import androidx.test.ext.junit.runners.AndroidJUnit4;

import androidx.test.rule.provider.ProviderTestRule;

import org.coursera.sustainableapps.capstone_project.provider.ContentProviderforURL;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import static junit.framework.Assert.assertNotNull;
import static org.coursera.sustainableapps.capstone_project.provider.ContentProviderforURL.AUTHORITY;
import static org.junit.Assert.assertEquals;

//Check the content content provider whether it works as expected or not.
@RunWith(AndroidJUnit4.class)
public class ContentProviderforURLTest   {
    private ContentResolver resolver;
    private  String URL1="content://" + AUTHORITY + "/downloaded_items";
    private String URL2="content://" + AUTHORITY + "/downloaded_items/121";




    private ContentValues[] contentValuesgenerator(int amount){
        ContentValues[] generate=new ContentValues[amount];
        for (int i=0;i<amount;i++){
            ContentValues mContentValues=new ContentValues();
            generate[i]=new ContentValues();
            int s1=i;
            int s2=i+100;
            generate[i].put(ContentProviderforURL.FILENAME,s1);
            generate[i].put(ContentProviderforURL.DOWNLOADTIME, s2);
        }
        return generate;
    }


    @Rule
    public ProviderTestRule mProviderRule =
            new ProviderTestRule.Builder(ContentProviderforURL.class, AUTHORITY).build();

    @Before
    public void setUp() throws Exception {
        resolver = mProviderRule.getResolver();
    }

    @Test
    public void checkinsertanddeletemethod() {
        //delete all at first
        resolver.delete(ContentProviderforURL.CONTENT_URI,null,null);
        //Create 20 content values array
        ContentValues[] tobeinserted=contentValuesgenerator(20);

        // Insert these values to the database
        for(ContentValues values:tobeinserted){
            resolver.insert(ContentProviderforURL.CONTENT_URI,values);
        }
        Cursor c = resolver.query(ContentProviderforURL.CONTENT_URI, null, null, null, null);
        assertNotNull(c);
        assertEquals(20, c.getCount());
        c.close();
        assertEquals(resolver.delete(ContentProviderforURL.CONTENT_URI,null,null),20);
        resolver.delete(ContentProviderforURL.CONTENT_URI,null,null);

        }
    @Test
    public void checkquerymethod() {
        resolver.delete(ContentProviderforURL.CONTENT_URI,null,null);
        //Create 20 content values array
        ContentValues[] tobeinserted=contentValuesgenerator(40);

        // Insert these values to the database
        for(ContentValues values:tobeinserted){
            resolver.insert(ContentProviderforURL.CONTENT_URI,values);
        }
        Cursor c = resolver.query(ContentProviderforURL.CONTENT_URI, null, null, null, null);
        int i=0;
        //Retrieve the inserted values and check whether these values matche the ones that have been inserted.
        if (c.moveToFirst()) {
            do {
                assertEquals(c.getInt(c.getColumnIndex(ContentProviderforURL.FILENAME)),i);
                assertEquals(c.getInt(c.getColumnIndex(ContentProviderforURL.DOWNLOADTIME)),i+100);
                i++;
            }
            while (c.moveToNext());
        }
        assertEquals(c.getCount(),40);
        resolver.delete(ContentProviderforURL.CONTENT_URI,null,null);
    }
    //Check whether getType method returns correct type or not.
    @Test
    public void testgetTypeMethod(){
        assertEquals(resolver.getType(Uri.parse(URL1)),"vnd.android.cursor.dir/vnd.example.downloaded_items");
        assertEquals(resolver.getType(Uri.parse(URL2)),"vnd.android.cursor.item/vnd.example.downloaded_items");
    }

    //Check whether updatetest method works as intended or not
    @Test
    public void testUpdateMethod(){
        resolver.delete(ContentProviderforURL.CONTENT_URI,null,null);
        //Create 20 content values array
        ContentValues[] tobeinserted=contentValuesgenerator(40);
        for(ContentValues values:tobeinserted){
            resolver.insert(ContentProviderforURL.CONTENT_URI,values);
        }
        //Change the values stored in the row where Filename is equal to 32
        String where1=ContentProviderforURL.FILENAME+"=32";
        ContentValues cnt=new ContentValues();
        cnt.put(ContentProviderforURL.FILENAME,864);
        cnt.put(ContentProviderforURL.DOWNLOADTIME,964);
        resolver.update(ContentProviderforURL.CONTENT_URI,cnt,where1,null);
        String where2=ContentProviderforURL.FILENAME+"=864";
        Cursor c=resolver.query(ContentProviderforURL.CONTENT_URI,null,where2,null,null);
        c.moveToLast();
        //Check whether the values are updated or not.
        assertEquals(c.getInt(c.getColumnIndex(ContentProviderforURL.FILENAME)),864);
        assertEquals(c.getInt(c.getColumnIndex(ContentProviderforURL.DOWNLOADTIME)),964);
        resolver.delete(ContentProviderforURL.CONTENT_URI,null,null);
    }

    //Check whether delete method works as intended or not
    @Test
    public void testDeleteMethod(){
        resolver.delete(ContentProviderforURL.CONTENT_URI,null,null);
        //Create 20 content values array
        ContentValues[] tobeinserted=contentValuesgenerator(40);
        for(ContentValues values:tobeinserted){
            resolver.insert(ContentProviderforURL.CONTENT_URI,values);
        }
        //First assure that cursor gets the row with filename=32
        String where1=ContentProviderforURL.FILENAME+"=32";
        Cursor c1=resolver.query(ContentProviderforURL.CONTENT_URI,null,where1,null,null);
        c1.moveToLast();
        assertEquals(c1.getCount(),1);
        //Then make sure that the count that cursor gets is zero after the deletion of the row.
        resolver.delete(ContentProviderforURL.CONTENT_URI,where1,null);
        Cursor c2=resolver.query(ContentProviderforURL.CONTENT_URI,null,where1,null,null);
        assertEquals(c2.getCount(),0);

    }


    }






