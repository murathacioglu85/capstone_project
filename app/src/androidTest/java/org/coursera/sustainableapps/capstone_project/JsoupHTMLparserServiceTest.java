package org.coursera.sustainableapps.capstone_project;

import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.rule.ServiceTestRule;

import org.coursera.sustainableapps.capstone_project.service.DownloadURLservice;
import org.coursera.sustainableapps.capstone_project.service.JsoupHTMLparser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static android.os.Looper.getMainLooper;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

public class JsoupHTMLparserServiceTest {
    private String avalidURL="https://en.wikipedia.org/wiki/List_of_New_York_area_codes";
    private String anInvalidURL="https://www.asedas";
    private JsoupHTMLparser service;
    @Rule
    public final ServiceTestRule serviceRule = new ServiceTestRule();
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        // Create the service Intent.
        Intent serviceIntent =
                new Intent(ApplicationProvider.getApplicationContext(),
                        JsoupHTMLparser.class);


        // Bind the service and grab a reference to the binder.
        IBinder binder = serviceRule.bindService(serviceIntent);

        // Get the reference to the service, or you can call
        // public methods on the binder directly.
        service = ((JsoupHTMLparser.LocalBinder) binder).getService();

    }

    @Test
    public void JSoupboundServicecheckwhethergetParsedMethodworks() throws Exception {
        // Verify that the service is working correctly.
        assertNotNull(service.getParsedHTMLdoc(avalidURL));
    }
}
