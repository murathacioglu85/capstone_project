package org.coursera.sustainableapps.capstone_project;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.rule.ActivityTestRule;

import org.coursera.sustainableapps.capstone_project.activities.DownloadURL;
import org.coursera.sustainableapps.capstone_project.activities.WebScraping;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class WebScrapingTest {
    private String defaulturl="https://en.wikipedia.org/wiki/List_of_New_York_area_codes";

    @Rule
    public ActivityTestRule<WebScraping> mActivityRule=new ActivityTestRule<>(WebScraping.class);

    @Before
    public void setUp(){
        mActivityRule.getActivity();
    }

    @Test
    public void checkdefaultURL(){
        //Check whether the text field is visible or not and it matches the default value.
        onView(withId(R.id.editURI)).check(matches(isDisplayed()));
        onView(withId(R.id.editURI)).check(matches(withText(defaulturl)));
    }
    @Test
    public void checksetOnTouchListener(){
        //Check whether the text field is cleared when any movement on the text field is detected.
        onView(withId(R.id.editURI)).perform(swipeUp());
        onView(withId(R.id.editURI)).check(matches(withText("")));
    }
    @Test
    public void checkextractTableButton(){
        //Check whether the text extracttable button works or not
        IdlingRegistry.getInstance().register(mActivityRule.getActivity().idlingResource);
        onView(withText("Dropdown Menu")).check(matches(isDisplayed()));
        onView(withId(R.id.button6)).perform(click());
        onView(withText("select")).check(matches(isDisplayed()));
        onView(withId(R.id.spinner2)).perform(click());
        onView(withText("table1")).check(matches(isDisplayed()));
        onView(withText("table2")).check(matches(isDisplayed()));
        onView(withText("table3")).check(matches(isDisplayed()));
        onView(withText("table4")).check(matches(isDisplayed()));
    }
    @Test
    public void checkDropDownMenu(){
        //check the dropdown menu. Click on each item to extract tables and ensure
        //the tables are retrieved on the screen.
        IdlingRegistry.getInstance().register(mActivityRule.getActivity().idlingResource);
        onView(withId(R.id.button6)).perform(click());
        onView(withId(R.id.spinner2)).perform(click());
        onView(withText("table1")).perform(click());
        onView(withId(R.id.listMode)).check(matches(isDisplayed()));
        onView(withId(R.id.spinner2)).perform(click());
        onView(withText("table2")).perform(click());
        onView(withId(R.id.listMode)).check(matches(isDisplayed()));
        onView(withId(R.id.spinner2)).perform(click());
        onView(withText("table3")).perform(click());
        onView(withId(R.id.listMode)).check(matches(isDisplayed()));
        onView(withId(R.id.spinner2)).perform(click());
        onView(withText("table4")).perform(click());
        onView(withId(R.id.listMode)).check(matches(isDisplayed()));
    }







}
