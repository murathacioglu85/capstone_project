package org.coursera.sustainableapps.capstone_project;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


import androidx.test.espresso.Espresso;
import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.IdlingResource;
import androidx.test.espresso.PerformException;
import androidx.test.espresso.Root;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;


import org.coursera.sustainableapps.capstone_project.activities.DownloadURL;
import org.coursera.sustainableapps.capstone_project.activities.MainActivity;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isFocused;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */


@RunWith(AndroidJUnit4.class)
public class DownloadURLTest {
    private String defaulturl="https://gitlab.com/murathacioglu85/capstone_project/-/raw/fb09256e60ed3a53d2ee124be3bc66518cb9261e/README.pdf";

    @Rule
    public ActivityTestRule<DownloadURL> mActivityRule=new ActivityTestRule<>(DownloadURL.class);

    @Before
    public void setUp(){
        mActivityRule.getActivity();
    }

    @Test
    public void checkdefaultURL(){
        //Check whether the text field is visible or not and it matches the default value.
        onView(withId(R.id.editURI)).check(matches(isDisplayed()));
        onView(withId(R.id.editURI)).check(matches(withText(defaulturl)));
    }
    @Test
    public void checksetOnTouchListener(){
        //Check whether the text field is cleared when any movement on the text field is detected.
        onView(withId(R.id.editURI)).perform(swipeUp());
        closeSoftKeyboard();
        onView(withId(R.id.editURI)).check(matches(withText("")));
    }
    @Test
    public void checkDownloadButton() {
        //Check whether the download button works as intended or not.
        IdlingRegistry.getInstance().register(mActivityRule.getActivity().idlingResource);
        onView(withId(R.id.button3)).perform(click());
        //Check whether snackbar pops up or not and click on OPEN on snackbar
        onView(withText("The item has been downloaded")).check(matches((isDisplayed())));
        onView(withText("OPEN")).perform(click());
    }

    @Test
    public void checkGenerateButton() {
        //Check whether the generate txt button works as intended or not.
        IdlingRegistry.getInstance().register(mActivityRule.getActivity().idlingResource);
        onView(withId(R.id.button4)).perform(click());
        onView(withText("The downloadhistory.txt file has been generated.")).check(matches((isDisplayed())));
        onView(withText("OPEN")).perform(click());
    }
    @Test
    public void checkViewHistoryButton(){
        //Check whether the viewhistory button works as intended or not.
        onView(withId(R.id.button5)).perform(click());
        //check whether message window is not empty
        onView(withId(R.id.messageWindow)).check(matches(not(withText(""))));
    }

}