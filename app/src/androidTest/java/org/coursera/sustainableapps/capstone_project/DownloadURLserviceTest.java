package org.coursera.sustainableapps.capstone_project;

import android.content.Intent;
import android.os.Handler;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.rule.ServiceTestRule;
import org.coursera.sustainableapps.capstone_project.service.DownloadURLservice;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.os.Looper.getMainLooper;
import static junit.framework.Assert.assertNotNull;

public class DownloadURLserviceTest {

    private DownloadURLservice mDUS;
    private Handler mHandler;

    @Rule
    public final ServiceTestRule serviceRule = new ServiceTestRule();


    @Before
    public void setUp() throws Exception {
        mDUS =  new DownloadURLservice();
        mHandler = new Handler(getMainLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {

            }
        });

    }

    /**
     * Test makeintent method and check whether the service starts or not.
     * @throws Exception
     */
    @Test
    public void checkmakeintentmethod() throws Exception {
        Intent intent=DownloadURLservice.makeIntent(ApplicationProvider.getApplicationContext(),"https://...","filename.txt",mHandler);
        assertNotNull(intent);
        assertNotNull(ApplicationProvider.getApplicationContext().startService(intent));
    }


}
