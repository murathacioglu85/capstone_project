package org.coursera.sustainableapps.capstone_project;

import android.content.Context;


import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;


import org.coursera.sustainableapps.capstone_project.activities.MainActivity;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule=new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp(){
        mActivityRule.getActivity();
    }

    @Test
    public void performClickonDownloadWithURLButton() {
        // Perform click on DownloadWithURLButton

        onView(withId(R.id.button)).check(matches(isClickable())).perform(click());
    }
    @Test
    public void performClickonWebScrapingButton() {
        // Perform click on buttons
        onView(withId(R.id.button2)).check(matches(isClickable())).perform(click());
    }

}