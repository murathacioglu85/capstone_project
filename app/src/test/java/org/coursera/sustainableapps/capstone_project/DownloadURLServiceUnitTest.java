package org.coursera.sustainableapps.capstone_project;

import org.coursera.sustainableapps.capstone_project.service.DownloadURLservice;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
;

/**
 * This Unit test has been prepared to test the helper methods in the downloadURL Service
 *
 */

public class DownloadURLServiceUnitTest {

    private DownloadURLservice downloadURLservice;
    private String avalidURL="https://upload.wikimedia.org/wikipedia/commons/3/3e/Einstein_1921_by_F_Schmutzer_-_restoration.jpg";
    private String anInvalidURL="https://www.asedas";



    @Before

    public void setUp(){
        downloadURLservice=new DownloadURLservice();
    }


    @Test
    public void checkURLmethodwithvalidURL() throws Exception{
        Boolean x=downloadURLservice.CheckURL(avalidURL);
        assertTrue(x);
    }
    @Test
    public void checkURLmethodwithinvalidURL() throws Exception{
        Boolean x=downloadURLservice.CheckURL(anInvalidURL);
        assertFalse(x);
    }
}
