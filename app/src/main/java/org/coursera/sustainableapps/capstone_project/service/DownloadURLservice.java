package org.coursera.sustainableapps.capstone_project.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;

import org.coursera.sustainableapps.capstone_project.activities.DownloadURL;
import org.coursera.sustainableapps.capstone_project.activities.MainActivity;
import org.coursera.sustainableapps.capstone_project.provider.ContentProviderforURL;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * <p>
 *
 * helper methods.
 */
public class DownloadURLservice extends IntentService {
    private int result= Activity.RESULT_CANCELED;
    public static final String URL="urlpath";
    public static final String FILENAME="filename";
    public static final String FILEPATH = "filepath";
    public static final String RESULT = "result";
    public static final String NOTIFICATION = "service receiver";
    private static final String MESSENGER_KEY = "MESSENGER_KEY";
    private static final String TAG= DownloadURLservice.class.getCanonicalName();



    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     *
     */
    public DownloadURLservice(){
        super("DownloadURLservice");
    }

    public static Intent makeIntent(Context context, String url, String filename, Handler
            downloadHandler) {
        // Create an intent that will download the image from the web.
        // which involves (1) setting the URL as "data" to the
        // intent, (2) putting the request code as an "extra" to the
        // intent, (3) creating and putting a Messenger as an "extra"
        // to the intent so the DownloadAtomFeedService can send the
        // Entry Object back to the Calling Activity
        Intent intent=new Intent(context, DownloadURLservice.class);
        intent.putExtra(URL,url);
        intent.putExtra(FILENAME,filename);
        intent.putExtra(MESSENGER_KEY, new Messenger(downloadHandler));
        return intent;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onHandleIntent(Intent intent) {
        Log.d("Service","service started");
        String urlPath = intent.getStringExtra(URL);
        String fileName = intent.getStringExtra(FILENAME);
        File output = new File(getExternalFilesDir(null),fileName);
        Bundle bundle=intent.getExtras();
        Messenger ms= (Messenger) bundle.get(MESSENGER_KEY);
        try {
            download(output,urlPath,fileName,ms);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public boolean CheckURL(String urlpath)  {

        URL url= null;
        try {
            url = new URL(urlpath);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        }


        URL finalUrl = url;
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ((HttpURLConnection) finalUrl.openConnection()).getResponseCode();
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
        });

        t.start();
        long endTimeMillis = System.currentTimeMillis() + 4000;
        while (t.isAlive()) {
            if (System.currentTimeMillis() > endTimeMillis) {
                t.interrupt();
                return false;
            }
        }
        try {
            int code = ((HttpURLConnection) url.openConnection()).getResponseCode();
            if(code == 200) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public void download(File output, String urlPath, String fileName, Messenger ms) throws Exception {
        if (!fileName.isEmpty()&&CheckURL(urlPath)){ File[] listofFiles=output.getParentFile().listFiles();
            Log.d("directory",output.getPath());
            Handler mHandler = new Handler(getMainLooper());
            mHandler.post(new Runnable() {
                              @Override
                              public void run() {
                                  Toast.makeText(DownloadURLservice.this, "download started", Toast.LENGTH_SHORT).show();
                              }
                          });

            checkHaMeRThreadNotInterrupted();
            if (output.exists()) {
                int count=0;
                for (File fl:listofFiles) {
                    if (fl.getName().contains(output.getName().substring(0,output.getName().indexOf(".")-1))) count++;
                }
                Log.d("directory",Integer.toString(count));

                fileName=fileName.substring(0,fileName.indexOf('.'))+"("+ (count) +")"+fileName.substring(fileName.indexOf('.'));

            }
            File newFile = new File(output.getParent(), fileName);
            try {
                Files.move(output.toPath(), newFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }


            try (BufferedInputStream in= new BufferedInputStream(new URL(urlPath).openStream());FileOutputStream fileOutputStream = new FileOutputStream(output.getPath())){
                byte dataBuffer []=new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                    fileOutputStream.write(dataBuffer, 0, bytesRead);
                }
                result = Activity.RESULT_OK;
                publishResults(output.getPath(), result);
                Insertdatatosqldatabase(fileName);
                checkHaMeRThreadNotInterrupted();
                sendMessage(ms,fileName,result);
            }
            catch (IOException e){
                e.printStackTrace();
                result=Activity.RESULT_CANCELED;
                publishResults(output.getPath(), result);

            }}
        else publishResults(output.getPath(), result);
    }

    private void sendMessage(Messenger ms,String filename,int result)  {
        Message message=makereply(filename,result);
        try {
            // Send the path to the image file back to the MainActivity.
            Log.d(TAG, "sending " + filename+ " back to the " +
                    "DownloadURLActivity");
            ms.send(message);;


        } catch (Exception e) {
            Log.e(getClass().getName(), "Exception while sending reply message back to DownloadURLActivity.",
                    e);
        }


    }
    private void checkHaMeRThreadNotInterrupted() throws Exception {
        if (Thread.currentThread().isInterrupted()) {
            Log.d(TAG, "HaMer thread interrupted");
            throw new Exception("HaMer thread interrupted, halting execution.");
        }
    }


    public Message makereply(String filename, int resultcode){
        Message message=Message.obtain();
        Bundle data=new Bundle();
        data.putInt(RESULT,resultcode);
        data.putString(FILENAME,filename);
        message.setData(data);
        return message;
    }



    public void Insertdatatosqldatabase(String fileName){
        ContentValues values=new ContentValues();
        values.put(ContentProviderforURL.FILENAME,fileName);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        values.put(ContentProviderforURL.DOWNLOADTIME, timestamp.toString());
        Uri uri = getContentResolver().insert(
                ContentProviderforURL.CONTENT_URI, values);
        Handler mHandler = new Handler(getMainLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(),
                        uri.toString(), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void publishResults(String outputPath, int result) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(FILEPATH, outputPath);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }

}


