package org.coursera.sustainableapps.capstone_project.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import org.coursera.sustainableapps.capstone_project.R;

/**
 * This activity is created to navigate to the two other features of this app.
 * Two buttons are shown on the screen:
 * "b1 is to open the "DownloadURL" activity and b2 is for "WebScraping" activity
 */

public class MainActivity extends AppCompatActivity {
    private Button b1;
    private Button b2;
    /**
     * String used for logging.
     */
    private static final String TAG = MainActivity.class.getCanonicalName();

    /**
     * Android Activty lifecycle methods.
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "activity created");
        setContentView(R.layout.activity_main);
        initializeUI();

    }

    /**
     * Helper method to initialize the UI. Mainly two buttons are shown on the screen and each
     * button navigates to two differenet activity.
     */
    private void initializeUI(){
        b1=findViewById(R.id.button);
        b2=findViewById(R.id.button2);
        b1.setOnClickListener(v -> {
            Intent intent=new Intent(this,DownloadURL.class);
            startActivity(intent);
        });
        b2.setOnClickListener(v -> {
            Intent intent=new Intent(this,WebScraping.class);
            startActivity(intent);
        });
    }


}