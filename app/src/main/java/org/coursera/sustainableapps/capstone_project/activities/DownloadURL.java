package org.coursera.sustainableapps.capstone_project.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import androidx.test.espresso.idling.CountingIdlingResource;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.provider.DocumentsContract;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.coursera.sustainableapps.capstone_project.R;
import org.coursera.sustainableapps.capstone_project.provider.ContentProviderforURL;
import org.coursera.sustainableapps.capstone_project.receivers.DownloadReceiver;
import org.coursera.sustainableapps.capstone_project.service.DownloadURLservice;
import org.coursera.sustainableapps.capstone_project.service.ResultfromServiceHandler;
import org.coursera.sustainableapps.capstone_project.service.ServiceResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;

/**
 * This activity uses intent service to download a file given by a URL and retrieves the download history data
 * form the datatbase, generates a history txt and shows the download history on the screen.
 */

public class DownloadURL extends AppCompatActivity implements ServiceResult  {

    /**
     * Activty instance variables
     */
    /**
     * String used for logging.
     */
    private static final String TAG = DownloadURL.class.getCanonicalName();

    public  CountingIdlingResource idlingResource=new CountingIdlingResource("Download");

    /**
     * this field is reserved for entering the URL
     */
    private TextView enterurl;
    /**
     * this field is reserved for the button to download the file given by the URL
     */
    private Button download;

    /**
     * this field is reserved for the button to generate a txt file
     */
    private Button generate;
    /**
     * this field is reserved forthe button to retrieve  the download history data to be shown on the screen
     */
    private Button viewhistory;
    /**
     * scrollView has been introduced to allow history view to be seen in full.
     */

    private ScrollView scrollView;
    /**
     * This is the field where the download history is shown.
     */
    private TextView histroytext;
    /**
     * This is the broadcast receiver instance. Upon completion of the download by the intent service, this receiver picks up
     * the message and show it on the screen.
     */
    private DownloadReceiver receiver = new DownloadReceiver();
    /**
     * Default URL for the enterurl text field.
     */

    private String defaulturl="https://gitlab.com/murathacioglu85/capstone_project/-/raw/fb09256e60ed3a53d2ee124be3bc66518cb9261e/README.pdf";
    private CoordinatorLayout coordinatorLayout;
    private Handler mServiceResultHandler = null;
    private int REQUEST_DOWNLOAD=1;

    /**
     * Android Activity lifecycle methods.
     * in onCreate hook method, this activity initializes the UI.
     * @param savedInstanceState
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloadurl);
        enterurl=findViewById(R.id.editURI);
        enterurl.setVisibility(View.VISIBLE);
        enterurl.setText(defaulturl);
        download=findViewById(R.id.button3);
        generate=findViewById(R.id.button4);
        viewhistory=findViewById(R.id.button5);
        histroytext=findViewById(R.id.messageWindow);
        coordinatorLayout=findViewById(R.id.coordinatorLayout);
        mServiceResultHandler = new ResultfromServiceHandler(this);
        Log.d(TAG,"Activity created");
        initializeUI();
    }

    /**
     * Helper method for initialization. Listeners have been introduced here for the buttons and text fields.
     */

    private void initializeUI() {
        enterurlOnTouch();
        downloadonClick();
        generateonClick();
        viewhistoryonClick();


    }
    /**
     * setOnTouchListener here is used to clear the text view upon any movement in the text field.
     *
     */
    private void enterurlOnTouch(){

        enterurl.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_MOVE){
                    enterurl.setText("");
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * setOnClickLister for download here introduced to initiate the download by sending the intent to the
     * intent service including the URL data
     */
    private void downloadonClick(){

        download.setOnClickListener(v -> {
            idlingResource.increment();
            String[] arr = enterurl.getText().toString().split("/");
            String fname = arr[arr.length - 1];
            Log.d(TAG, fname);
            Intent intent = DownloadURLservice.makeIntent(this,enterurl.getText().toString(),fname,mServiceResultHandler);
            startService(intent);

        });
    }
    /**
     * This creates a new txt file and writes the data obtained from the content provider into the
     * downloadhistory.txt file.
     *
     */

    private void generateonClick(){

        generate.setOnClickListener(v -> {
            idlingResource.increment();
            File output = new File(getExternalFilesDir(null), "downloadhistory.txt");
            for (File cc : output.getParentFile().listFiles()) {
                if (cc.getName().equals(output.getName()))
                    cc.delete();
            }
            FilewriterforDownloadhistory(output);
        });
    }



    private void FilewriterforDownloadhistory(File output){
        // query for the all items and iterate over these items to write them into the file
        Cursor c = getContentResolver().query(ContentProviderforURL.CONTENT_URI, null, null, null, null);
        Toast.makeText(this, "downloadhistory.txt file generated", Toast.LENGTH_SHORT).show();
        if (c.moveToFirst()) {
            do {
                FileWriter writer = null;
                try {
                    writer = new FileWriter(output,true);
                    String s = c.getString(c.getColumnIndex(ContentProviderforURL._ID)) +
                            ", " + c.getString(c.getColumnIndex(ContentProviderforURL.FILENAME)) +
                            ", " + c.getString(c.getColumnIndex(ContentProviderforURL.DOWNLOADTIME));
                    writer.append(s+"\n\n");
                    writer.flush();
                    writer.close();
                    Log.d(TAG,s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            while (c.moveToNext());
            //toast a message on the screen upon completion.
            Toast.makeText(this, "sql data written in downloadhistory.txt file", Toast.LENGTH_SHORT).show();
            try {
                Thread.currentThread().sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // Snackbar pops up after a while to synchronize it with toast
            createSnackbar2(output);

        }
    }
    /**
     * viewhistory button does the same as the generate button except writing data into a txt file
     * Instead, the data is shown on the screen in a reserved text field.
     *
     */
    private void viewhistoryonClick(){
        viewhistory.setOnClickListener(v -> {
            Cursor c = getContentResolver().query(ContentProviderforURL.CONTENT_URI, null, null, null, null);
            StringBuilder str= new StringBuilder();
            if (c.moveToFirst()) {
                do {
                    TextView tv = new TextView(this);
                    tv.setText("Your string");
                    String s = c.getString(c.getColumnIndex(ContentProviderforURL._ID)) +
                            ", " + c.getString(c.getColumnIndex(ContentProviderforURL.FILENAME)) +
                            ", " + c.getString(c.getColumnIndex(ContentProviderforURL.DOWNLOADTIME))+"\n";
                    str.append(s);
                }
                while (c.moveToNext());
            }
            c.close();
            //set the text for the screen. This field is scrollable both horizontally and vertically.
            histroytext.setText(str.toString());
        });
    }



    /**
     * while on resume, brodcast receiver is registered and receives the intent sent by the service
     * to publish the download results.
     */
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(DownloadURLservice.NOTIFICATION));

    }

    /**
     * Helper method to open the file
     * @param Filename: name of the file
     */
    private void getFileandopenFile(String Filename){

            File output = new File(getExternalFilesDir(null), Filename);
            createSnackbar(output);
    }

    /**
     * Snackbar pops up upon completion of the download.
     * This one is reserved for downloaded items.
     * @param output:
     */

    private void createSnackbar(File output){
        Snackbar snackbar=Snackbar.make(coordinatorLayout, "The item has been downloaded", BaseTransientBottomBar.LENGTH_INDEFINITE).setAction("OPEN", v -> openfile(output));
        snackbar.show();
        idlingResource.decrement();
    }

    /**
     * This snackbar pops up upon completion of downloadhistory.txt generation.
     * @param output:File to be opened.
     */
    private void createSnackbar2(File output){
        Snackbar snackbar=Snackbar.make(coordinatorLayout, "The downloadhistory.txt file has been generated.", BaseTransientBottomBar.LENGTH_INDEFINITE).setAction("OPEN", v -> openfile(output));
        snackbar.show();
        idlingResource.decrement();
    }

    /**
     * File Provider class has been used to get access the downloaded documents.
     * tHis helper method is used to open the downloaded item.
     * @param output:File to be opened
     */

    private void openfile(File output){
        Intent intent1 = new Intent(Intent.ACTION_VIEW);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        String extension = MimeTypeMap.getFileExtensionFromUrl(output.getPath());
        String type=MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkURI = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".provider", output);
            intent1.setDataAndType(apkURI, type);
            intent1.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            intent1.setDataAndType(Uri.fromFile(output), type);
        }
        startActivity(intent1);
    }
    /**
     * while on pause, brodcast receiver is unregistered.
     */
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    /**
     * Upon receiving the message, this method is implemented.
     * @param resultCode:resultcode to proceed further
     * @param data: data to be extracted and processed
     */


    @Override
    public void onServiceResult(int resultCode, Bundle data) {
        if (resultCode==Activity.RESULT_OK){
            String filename=data.getString(DownloadURLservice.FILENAME);
            getFileandopenFile(filename);
        }
    }
}