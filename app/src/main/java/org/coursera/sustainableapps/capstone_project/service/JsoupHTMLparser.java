package org.coursera.sustainableapps.capstone_project.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

/**
 * This service is used to parse the html page and extract the tables
 * from the given web page
 */

public class JsoupHTMLparser extends Service {

    private final IBinder binder = new LocalBinder();
    public JsoupHTMLparser() {
    }


    public class LocalBinder extends Binder {
        public JsoupHTMLparser getService() {
            // Return this instance of LocalService so clients can call public methods
            return JsoupHTMLparser.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * This is the method which is invoked by the client to parse web site and extract the data.
     * @param url
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public Document getParsedHTMLdoc(String url) throws ExecutionException, InterruptedException {

        Callable<Document> getDoc=new Callable<Document>() {
            @Override
            public Document call() throws Exception {
                return Jsoup.connect(url).get();
            }
        };
        return Executors.newSingleThreadExecutor().submit(getDoc).get();
    }
}