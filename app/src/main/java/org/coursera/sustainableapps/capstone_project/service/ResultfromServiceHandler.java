package org.coursera.sustainableapps.capstone_project.service;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.lang.ref.WeakReference;

/**
 * ServiceHandler to be used to provide communication between DownloadURLservice
 * and DownloadURL activity.
 */

public class ResultfromServiceHandler extends Handler {

    private final String TAG = getClass().getSimpleName();
    private WeakReference<ServiceResult> mResult;
    public ResultfromServiceHandler(ServiceResult serviceResult) {
        mResult = new WeakReference<>(serviceResult);
    }
    public void onConfigurationChange(ServiceResult serviceResult) {
        mResult = new WeakReference<>(serviceResult);
    }

    /**
     * Data attached to the message are extracted by the method below and
     * the thread is put to sleep for 5 seconds for snackbar to pop up right after the toast messages.
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {
        Log.d(TAG,
                "handleMessage() called back");
        final Bundle data = message.getData();
        final int resultCode = data.getInt(DownloadURLservice.RESULT);
        if (mResult.get() == null) {
            // Warn programmer that mResult callback reference has
            // been lost without being restored after a configuration
            // change.
            Log.w(TAG, "Configuration change handling not implemented correctly;"
                    + " lost weak reference to ServiceResult callback)");
        } else {
            // Forward result to callback implementation.
            try {
                Thread.currentThread().sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mResult.get().onServiceResult(resultCode, data);
        }
    }
}
