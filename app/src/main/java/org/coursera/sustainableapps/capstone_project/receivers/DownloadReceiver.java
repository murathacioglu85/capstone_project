package org.coursera.sustainableapps.capstone_project.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import org.coursera.sustainableapps.capstone_project.activities.DownloadURL;
import org.coursera.sustainableapps.capstone_project.service.DownloadURLservice;

/**
 * Broadcast Receiver to publish the download results and download URI.
 */

public class DownloadReceiver extends BroadcastReceiver {
    /**
     * Upon receiving the info form the service, the broadcast receiver publish the results with a toast
     * giving the downlaod status and the download URI.
     * @param context
     * @param intent
     */

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data=intent.getExtras();
        int resultcode=data.getInt(DownloadURLservice.RESULT);
        String flnm=data.getString(DownloadURLservice.FILEPATH);
        if (resultcode== Activity.RESULT_OK) {
            Toast.makeText(context,"Download complete. Download URI: " + flnm,
                Toast.LENGTH_SHORT).show();
        }
        else {

            Toast.makeText(context, "Download failed", Toast.LENGTH_LONG).show();
        }
    }

}