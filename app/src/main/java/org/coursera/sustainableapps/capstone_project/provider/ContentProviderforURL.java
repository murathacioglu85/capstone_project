package org.coursera.sustainableapps.capstone_project.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.HashMap;

/**
 * The content provider class is used to create a database for the download history.
 */
public class ContentProviderforURL extends ContentProvider {

    /**
     * Content Provider Authority
     */
    public static final String AUTHORITY="org.coursera.sustainableapps.capstone_project.PROVIDER";
    /**
     *URL of the content and then coverted to URI
     */
    public static final String URL="content://" + AUTHORITY + "/downloaded_items";
    public static final Uri CONTENT_URI = Uri.parse(URL);
    /**
     * UriMatcher used to to match the URIs in the content providers. For single items
     * DOWNLOADED_ITEMS_ID has been defined whereas the DOWNLOADED_ITEMS are reserved for
     * All downloaded ITEMS.
     *
     */
    private static final int DOWNLOADED_ITEMS = 1;
    private static final int DOWNLOADED_ITEMS_ID = 2;
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        uriMatcher.addURI(AUTHORITY, "downloaded_items", DOWNLOADED_ITEMS);
        uriMatcher.addURI(AUTHORITY, "downloaded_items/#", DOWNLOADED_ITEMS_ID);
    }

    /**
     * These are the variables required to create a sql database.
     * The column description, table name, column name(filename and download time), etc.
     */
    private SQLiteDatabase db;
    public static final String _ID = "_id";
    public static final String FILENAME = "file_name";
    public static final String DOWNLOADTIME = "download_time";
    public static final String DATABASE_NAME = "DOWNLOADS";
    public static final String DOWNLOAD_TABLE_NAME = "downloaded_items";
    public static final int DATABASE_VERSION = 1;
    private static HashMap<String, String> DOWNLOAD_PROJECTION_MAP;
    static final String CREATE_DB_TABLE =
            " CREATE TABLE " + DOWNLOAD_TABLE_NAME +
                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " file_name TEXT NOT NULL, " +
                    " download_time TEXT NOT NULL);";

    /**
     * Database helper class introduces to manage database creation and version management
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " +  DOWNLOAD_TABLE_NAME);
            onCreate(db);
        }
    }
    public ContentProviderforURL() {
    }

    /**
     * Based on the paramethers, the matching URI in the content provider is adressed
     * and the item in the database is deleted. Then, content resolver is called
     * to notify the change
     * @param uri
     * @param selection
     * @param selectionArgs
     * @return
     */

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // copy of whereClause to modify if needed.
        int count = 0;
        switch (uriMatcher.match(uri)){
            case DOWNLOADED_ITEMS:
                count = db.delete(DOWNLOAD_TABLE_NAME, selection, selectionArgs);
                break;

            case DOWNLOADED_ITEMS_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete( DOWNLOAD_TABLE_NAME, _ID +  " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    /**
     * The string representation of the URi type is returned.
     * @param uri
     * @return
     */

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){

            case DOWNLOADED_ITEMS:
                return "vnd.android.cursor.dir/vnd.example.downloaded_items";

            case DOWNLOADED_ITEMS_ID:
                return "vnd.android.cursor.item/vnd.example.downloaded_items";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    /**
     * Insertion takes place. the contentvalues are inserted to the sql database.
     * @param uri
     * @param values
     * @return
     */

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID = db.insert(	DOWNLOAD_TABLE_NAME, "", values);
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    /**
     * Database helper class is invoked to create database.
     * @return
     */

    @Override
    public boolean onCreate() {
        Context context = getContext();
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        return (db == null)? false:true;
    }

    /**
     * Query builder is invoked. by the help of the builder, the query is performed.
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     * @return
     */

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(DOWNLOAD_TABLE_NAME);
        switch (uriMatcher.match(uri)) {
            case DOWNLOADED_ITEMS:
                qb.setProjectionMap(DOWNLOAD_PROJECTION_MAP);
                break;

            case DOWNLOADED_ITEMS_ID:
                qb.appendWhere( _ID + "=" + uri.getPathSegments().get(1));
                break;

            default:
        }
        if (sortOrder == null || sortOrder == ""){
            sortOrder = DOWNLOADTIME;
        }
        Cursor c = qb.query(db,	projection,	selection,
                selectionArgs,null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    /**
     * update the table based on the given parameters.
     * @param uri
     * @param values
     * @param selection
     * @param selectionArgs
     * @return
     */

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case DOWNLOADED_ITEMS:
                count = db.update(DOWNLOAD_TABLE_NAME, values, selection, selectionArgs);
                break;

            case DOWNLOADED_ITEMS_ID:
                count = db.update(DOWNLOAD_TABLE_NAME, values,
                        _ID + " = " + uri.getPathSegments().get(1) +
                                (!TextUtils.isEmpty(selection) ? " AND (" +selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri );
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

}