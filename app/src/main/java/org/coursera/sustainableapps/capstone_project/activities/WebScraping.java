package org.coursera.sustainableapps.capstone_project.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.test.espresso.idling.CountingIdlingResource;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.coursera.sustainableapps.capstone_project.R;
import org.coursera.sustainableapps.capstone_project.service.JsoupHTMLparser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * This activity uses bound service to extract the table from a given URL
 */

public class WebScraping extends AppCompatActivity  {
    /**
     * String used for logging.
     */
    private static final String TAG = DownloadURL.class.getCanonicalName();
    /**
     * Text field for the URL to be scraped.
     */
    private TextView enterurl;
    /**
     * button to commence the process of table extraction.
     */
    private Button extracttables;
    /**
     * dropdown menu for the table list which has been created by the extracttables button.
     */
    private Spinner dropdown;
    /**
     * A default URL provided for the user.
     */
    private String defaulturl="https://en.wikipedia.org/wiki/List_of_New_York_area_codes";
    /**
     * Jsoup Document file fot the html to be parsed
     */
    public CountingIdlingResource idlingResource=new CountingIdlingResource("WebScraping");
    private Document doc;
    /**
     * If dropdown menu is empty, this text field is shown as "Dropdown Menu"
     */
    private TextView txt;
    /**
     * This field is reserved for the tables.
     */
    private TextView tv;
    /**
     * Service instance for HTML parsing and table extraction.
     */
    JsoupHTMLparser mService;
    Context context;

    private TableLayout mtableLayout;
    private HorizontalScrollView hrz;
    /**
     * It shows whether service is bound or not.
     */
    boolean mBound = false;
    /**
     * Android Activity lifecycle methods.
     * in onCreate hook method, this activity initializes the UI.
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_scraping);
        enterurl=findViewById(R.id.editURI);
        enterurl.setText(defaulturl);
        enterurl.setVisibility(View.VISIBLE);
        extracttables=findViewById(R.id.button6);
        dropdown=findViewById(R.id.spinner2);
        tv=findViewById(R.id.messageWindow);
        txt=findViewById(R.id.textView2);
        txt.setText("Dropdown Menu");
        hrz=findViewById(R.id.startHorizontal);
        mtableLayout=findViewById(R.id.listMode);
        context=getApplicationContext();
        Log.d(TAG,"Activity Created");
    }

    /**
     * As bound service is used, the listeners added inside the onStart() hook method.
     * An intent is created and this intent is passed to the bind service.By doing so, this activity can use
     * the methods created in the bind service class.
     */

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"Activity Started");
        Intent intent=new Intent(this, JsoupHTMLparser.class);
        bindService(intent,connection,Context.BIND_AUTO_CREATE);
        enterurlOnTouch();
        extractTablesonClick();
    }
    private void enterurlOnTouch(){

        enterurl.setOnTouchListener((v, event) -> {
            if(event.getAction() == MotionEvent.ACTION_MOVE){
                enterurl.setText("");
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
                return true;
            }
            return false;
        });
    }



    private void extractTablesonClick(){
        extracttables.setOnClickListener(v -> {
            String urlstr=enterurl.getText().toString();
            /**
             * Jsoup document is created here to utilize the Jsoup library to extract tables.
             * First a list is generated for tables to be shown in the dropdown menu and then
             * Upon selection of a specific table,  the table is shown on the screen.
             */
            if(mBound){
                try {
                    idlingResource.increment();
                    doc=mService.getParsedHTMLdoc(urlstr);
                    String[] arraySpinner=getDropdownitems(doc);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arraySpinner);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dropdown.setAdapter(adapter);
                    idlingResource.decrement();
                    int count = mtableLayout.getChildCount();
                    for (int i = 0; i < count; i++) {
                        View child = mtableLayout.getChildAt(i);
                        if (child instanceof TableRow) ((ViewGroup) child).removeAllViews();
                    }
                    /**
                     * setOnItemListener has been introduced to retrieve the data of a specific table to be shown on
                     * the screen.
                     */
                   dropdownMenuListener();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }});

    }
    private void dropdownMenuListener(){
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txt.setVisibility(View.GONE);
                if (position!=0){
                    int count = mtableLayout.getChildCount();
                    for (int i = 0; i < count; i++) {
                        View child = mtableLayout.getChildAt(i);
                        if (child instanceof TableRow) ((ViewGroup) child).removeAllViews();
                    }
                    createTable(doc.select("table").get(position-1));
                }
                else mtableLayout.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Not needed for this app.
            }
        });
    }


    private void createTable(Element table){
        TableLayout.LayoutParams params=new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        Elements rows=table.select("tr");
        int i=0;
        for (Element row: rows) {
            TableRow tr=new TableRow(WebScraping.this);
            if (i==0) tr.setBackgroundColor(Color.parseColor("#000000"));
            tr.setLayoutParams(params);
            Elements cols = row.select("td");
            Elements headercells=row.select("th");
            Log.d(TAG, "colnum:" +String.valueOf(headercells.size()+cols.size()));
            int j=0;
            for (Element headercell:headercells) {
                TextView x=new TextView(WebScraping.this);
                Log.d("WebScraping table",headercell.text());
                x.setText(headercell.text()+"\t");
                if(i!=0){if (j%2==0) {x.setBackgroundColor(Color.parseColor("#0000FF"));
                    x.setTextColor(Color.parseColor("#FFFF00"));}
                else {
                    x.setBackgroundColor(Color.parseColor(	"#008000"));
                    x.setTextColor(Color.parseColor("#0000FF"));
                }}
                else{x.setTextColor(Color.parseColor("#FFFFFF"));}
                tr.addView(x);
                j++;
            }
            for (Element col:cols) {
                TextView x=new TextView(WebScraping.this);
                Log.d("WebScraping table",col.text());
                x.setText(col.text()+"\t");
                if(i!=0){if (j%2==0) {x.setBackgroundColor(Color.parseColor("#0000FF"));
                    x.setTextColor(Color.parseColor("#FFFF00"));
                }
                else {x.setBackgroundColor(Color.parseColor(	"#008000"));
                x.setTextColor(Color.parseColor("#0000FF"));}}
                else{x.setTextColor(Color.parseColor("#FFFFFF"));}
                tr.addView(x);
                j++;
            }
            i++;
            mtableLayout.addView(tr);
        }
        mtableLayout.setVisibility(View.VISIBLE);
    }
    /**
     * Helper method to generate dropdown list.
     *  @param doc
     * @return
     */


    private String[] getDropdownitems(Document doc){
        String[] s = new String[doc.select("table").size()+1];
        for (int j=0;j<s.length;j++){
            if (j==0)s[j]="select";
            else s[j]="table"+(j);
        }
        return s;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mBound){
            unbindService(connection);
            mBound = false;


        }
    }

    /**
     * A service connection established to bind with the service
     */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            JsoupHTMLparser.LocalBinder binder = (JsoupHTMLparser.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


}